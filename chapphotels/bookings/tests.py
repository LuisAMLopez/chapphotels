import json
from chapphotels.test_base import TestsBase
from rooms.models import Room
from .models import Booking
from django.contrib.auth.models import User
from decimal import Decimal
from datetime import timedelta
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.urls import reverse


class Tests(TestsBase):
    def setUp(self):
        super(Tests, self).setUp()

    def test_booking_base_create(self):
        '''
            Test booking base create (model create)
        '''
        base_from_date = timezone.now().replace(hour=12, minute=0, second=0)
        base_until_date = base_from_date + timedelta(days=2)

        Booking.objects.create(
            room=self.room_1, user=self.user,
            from_date=base_from_date, until_date=base_until_date,
            paid=True, price=Decimal(500)
        )

        self.assertEquals(Booking.objects.count(), 1)
        self.assertIsNotNone(Booking.objects.first().booking_number)

        Booking.objects.create(
            room=self.room_2, user=self.user,
            from_date=base_from_date, until_date=base_until_date,
            paid=True, price=Decimal(500)
        )

        self.assertEquals(Booking.objects.count(), 2)
        self.assertIsNotNone(Booking.objects.filter(room=self.room_2).get().booking_number)

        # Check that bookings can not be created for booked rooms

        with self.assertRaises(ValidationError):
            Booking.objects.create(
                room=self.room_1, user=self.user,
                from_date=base_from_date + timedelta(days=1),
                until_date=base_until_date + timedelta(days=1),
                paid=True, price=Decimal(500)
            )

        with self.assertRaises(ValidationError):
            Booking.objects.create(
                room=self.room_1, user=self.user,
                from_date=base_from_date - timedelta(days=1),
                until_date=base_until_date - timedelta(days=1),
                paid=True, price=Decimal(500)
            )

        with self.assertRaises(ValidationError):
            Booking.objects.create(
                room=self.room_1, user=self.user,
                from_date=base_from_date,
                until_date=base_until_date,
                paid=True, price=Decimal(500)
            )

        with self.assertRaises(ValidationError):
            Booking.objects.create(
                room=self.room_1, user=self.user,
                from_date=base_from_date - timedelta(days=1),
                until_date=base_until_date,
                paid=True, price=Decimal(500)
            )

        with self.assertRaises(ValidationError):
            Booking.objects.create(
                room=self.room_1, user=self.user,
                from_date=base_from_date,
                until_date=base_until_date + timedelta(days=1),
                paid=True, price=Decimal(500)
            )

        with self.assertRaises(ValidationError):
            Booking.objects.create(
                room=self.room_1, user=self.user,
                from_date=base_from_date - timedelta(days=1),
                until_date=base_until_date + timedelta(days=1),
                paid=True, price=Decimal(500)
            )

        with self.assertRaises(ValidationError):
            Booking.objects.create(
                room=self.room_1, user=self.user,
                from_date=base_from_date + timedelta(days=1),
                until_date=base_until_date,
                paid=True, price=Decimal(500)
            )

        with self.assertRaises(ValidationError):
            Booking.objects.create(
                room=self.room_1, user=self.user,
                from_date=base_from_date + timedelta(days=1),
                until_date=base_until_date + timedelta(days=1),
                paid=True, price=Decimal(500)
            )

        self.assertEquals(Booking.objects.count(), 2)

        Booking.objects.create(
            room=self.room_1, user=self.user,
            from_date=base_from_date - timedelta(days=1),
            until_date=base_from_date,
            paid=True, price=Decimal(500)
        )

        self.assertEquals(Booking.objects.count(), 3)

        Booking.objects.create(
            room=self.room_1, user=self.user,
            from_date=base_until_date,
            until_date=base_until_date + timedelta(days=1),
            paid=True, price=Decimal(500)
        )

        self.assertEquals(Booking.objects.count(), 4)

    def test_booking_create_service(self):
        '''
            Test booking create web service behavior
        '''
        self.signin()

        exist_bookings = Booking.objects.count()

        request_data = {
            'from_date': timezone.now().strftime('%Y-%m-%d %H:%M:%S'),
            'until_date': (timezone.now() + timedelta(days=2)).strftime('%Y-%m-%d %H:%M:%S'),
            'room': self.room_1.pk,
            'phone': 634332435,
            'card_number': 665333432432,
            'address': 'Calle las palmas N 9 Portal 3',
            'observations': 'Extra observations'
        }

        response = self.client.post(reverse('booking-create'), request_data)
        self.assertRedirects(response, reverse('booking-list'))

        self.assertEquals(Booking.objects.count(), exist_bookings + 1)

        from_date = timezone.now().replace(hour=12, minute=0, second=0)
        until_date = (timezone.now() + timedelta(days=2)).replace(hour=12, minute=0, second=0)

        exist_booking_check = Booking.objects.filter(
            user=self.user, from_date=from_date,
            until_date=until_date
        ).exists

        self.assertTrue(exist_booking_check)

    def test_booking_create_service_failure(self):
        '''
            Test service without a session active do not create new booking
        '''
        exist_bookings = Booking.objects.count()

        request_data = {
            'from_date': timezone.now().strftime('%Y-%m-%d %H:%M:%S'),
            'until_date': (timezone.now() + timedelta(days=2)).strftime('%Y-%m-%d %H:%M:%S'),
            'room': self.room_1.pk,
            'phone': 634332435,
            'card_number': 665333432432,
            'address': 'Calle las palmas N 9 Portal 3',
            'observations': 'Extra observations'
        }

        response = self.client.post(reverse('booking-create'), request_data)
        self.assertRedirects(response, reverse('login'))

        self.assertEquals(Booking.objects.count(), exist_bookings)

    def test_booking_list_view(self):
        '''
            Test booking list web service behavior
        '''
        self.signin()

        self.create_bookings()

        expected_bookings = Booking.objects.filter(user=self.user)

        response = self.client.get(reverse('booking-list'))
        self.assertEquals(response.status_code, 200)

        bookings = response.context_data.get('bookings')
        self.assertEquals(bookings.count(), expected_bookings.count())

        for booking in bookings:
            expected_bookings.get(pk=booking.pk)

    def test_booking_detail_view(self):
        '''
            Test booking detail web service behavior
        '''
        self.signin()

        self.create_bookings()

        expected_booking = Booking.objects.filter(user=self.user).first()

        response = self.client.get(reverse('booking-detail', kwargs={'pk': expected_booking.pk}))
        self.assertEquals(response.status_code, 200)

        booking = response.context_data.get('booking')

        self.assertEquals(booking.pk, expected_booking.pk)
        self.assertEquals(booking.booking_number, expected_booking.booking_number)
        self.assertEquals(booking.room, expected_booking.room)
        self.assertEquals(booking.user, expected_booking.user)
        self.assertEquals(booking.from_date, expected_booking.from_date)
        self.assertEquals(booking.until_date, expected_booking.until_date)
        self.assertEquals(booking.paid, expected_booking.paid)
        self.assertEquals(booking.phone, expected_booking.phone)
        self.assertEquals(booking.card_number, expected_booking.card_number)
        self.assertEquals(booking.address, expected_booking.address)
        self.assertEquals(booking.observations, expected_booking.observations)
        self.assertEquals(booking.price, expected_booking.price)

    def test_booking_delete_view(self):
        '''
            Test booking delete web service behavior
        '''
        self.signin()

        self.create_bookings()

        booking_to_delete = Booking.objects.filter(user=self.user).first()
        bookings_before = Booking.objects.count()

        response = self.client.get(reverse('booking-delete', kwargs={'pk': booking_to_delete.pk}))
        self.assertRedirects(response, reverse('booking-list'))

        self.assertEquals(Booking.objects.count(), bookings_before - 1)

        with self.assertRaises(Booking.DoesNotExist):
            Booking.objects.get(pk=booking_to_delete.pk)
