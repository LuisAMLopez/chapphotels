from django.urls import path
from .views import (
	BookingCreateView, BookingListView, BookingDetailView, BookingDetailExportView,
	booking_delete_view
)

urlpatterns = [
	path('create/', BookingCreateView.as_view(), name="booking-create"),
	path('list/', BookingListView.as_view(), name="booking-list"),
	path('detail/<int:pk>/', BookingDetailView.as_view(), name='booking-detail'),
	path('detail/<int:pk>/export/', BookingDetailExportView.as_view(), name='booking-detail-export'),
	path('delete/<int:pk>/', booking_delete_view, name='booking-delete'),
]