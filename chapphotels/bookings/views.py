import io
import json
from django.shortcuts import render
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from .models import Booking
from .forms import BookingForm
from django.utils.decorators import method_decorator
from chapphotels.decorators import login_required
from django.core.serializers import serialize
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.template.loader import get_template
from django.utils import timezone
from xhtml2pdf import pisa
from wsgiref.util import FileWrapper
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _


class BookingCreateView(CreateView):
    model = Booking
    form_class = BookingForm
    template_name = None

    @method_decorator(login_required)
    def dispatch(self, request):
        return super(BookingCreateView, self).dispatch(request=request)

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline
        formsets with the passed POST variables and then checking them for
        validity.
        """
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        """
                Called if all forms are valid. Creates a Booking instance.
        """
        # ***********DistributorAdmin************
        self.object = form.save(commit=False)
        self.object.user = self.request.user

        try:
            self.object.save()
        except BaseException as e:
            raise Exception(e)

        return redirect('booking-list', permanent=False)

    def form_invalid(self, form):
        """
        Called if a form is invalid. Re-renders the context data with the
        data-filled forms and errors.
        """

        res = json.dumps({
            'success': False,
            'non_field_errors': form.non_field_errors(),
            'form_errors': form.errors
        })

        return HttpResponse(res, content_type='application/json')


class BookingListView(ListView):

    model = Booking
    context_object_name = 'bookings'
    template_name = 'bookings/booking_list.html'

    @method_decorator(login_required)
    def dispatch(self, request):
        return super(BookingListView, self).dispatch(request=request)

    def get_queryset(self):
        return super(BookingListView, self).get_queryset().filter(user=self.request.user)


class BookingDetailView(DetailView):
    model = Booking
    context_object_name = 'booking'
    template_name = 'bookings/booking_detail.html'

    @method_decorator(login_required)
    def dispatch(self, request, pk):
        return super(BookingDetailView, self).dispatch(request=request)

    def get_context_data(self, **kwargs):
        """
            Overwrite this method to set all bookings into context
        """
        context = {}
        if self.object:
            context['object'] = self.object
            context_object_name = self.get_context_object_name(self.object)
            if context_object_name:
                context[context_object_name] = self.object
        context.update(kwargs)
        context['bookings'] = Booking.objects.filter(user=self.request.user)
        return super(BookingDetailView, self).get_context_data(**context)


class BookingDetailExportView(BookingDetailView):
    def render_to_response(self, context, **response_kwargs):
        """
            Overwrite this method to return pdf file
        """
        html_pdf = get_template(self.template_name)
        pdf_name = 'reserva-' + self.object.booking_number
        file_name = pdf_name + timezone.now().strftime("%Y-%m-%d-%H-%M-%S") + '.pdf'
        name_file = 'pdf_files/' + file_name
        path_file = settings.MEDIA_ROOT + name_file

        context = {
            'booking': self.object
        }

        pdf_content = html_pdf.render(context)

        pdf_file = io.FileIO(path_file, "wb")
        pisa.CreatePDF(pdf_content, pdf_file)

        try:
            report_file = open(path_file, 'rb')
        except Exception as e:
            report_file = None

        response = HttpResponse(FileWrapper(report_file), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="{0}"'.format(file_name)

        return response


@login_required
def booking_delete_view(request, pk):
    if not request.method == 'GET':
        raise ValidationError(
            _('El método en el que se realiza esta petición no está permitido')
        )

    try:
        booking = Booking.objects.get(pk=pk)
        booking.delete()

    except Booking.DoesNotExist:
        raise ValidationError(
            _('No existe una reserva con código %s' % pk)
        )
    except Exception as e:
        raise e

    return redirect('booking-list', permanent=False)
