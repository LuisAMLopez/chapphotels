# -*- encoding: utf-8 -*-
import math
from django.db import models
from rooms.models import Room
from django.contrib.auth.models import User
from datetime import datetime
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.utils import timezone
from chapphotels.snippets import get_diff_between_tow_dates


class Booking(models.Model):
    booking_number = models.CharField(max_length=30, null=True, blank=True, unique=True)
    room = models.ForeignKey(Room, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    from_date = models.DateTimeField('Fecha de entrada')
    until_date = models.DateTimeField('Fecha de salida')
    paid = models.BooleanField('Pagado', default=False)
    phone = models.IntegerField('Teléfono', null=True, blank=True)
    card_number = models.IntegerField('Número de tarjeta', null=True, blank=True)
    address = models.CharField('Dirección', max_length=100, null=True, blank=True)
    observations = models.CharField('Dirección', max_length=200, null=True, blank=True)
    price = models.DecimalField('Precio de la reserva (€)', max_digits=19, decimal_places=2)

    class Meta:
        verbose_name_plural = "Reservas"
        verbose_name = "Reserva"

    def save(self, *args, **kwargs):
        # Populate the booking_number if it is missing
        if not self.booking_number:
            self.booking_number = datetime.now().strftime('%y%m%d%H%M%S%f') + str(self.room_id)

        # Check if the room is already booked, if it`s, return ValidationError
        from_date = self.from_date.replace(hour=12, minute=0, second=0)
        until_date = self.until_date.replace(hour=12, minute=0, second=0)

        from_in_range_filter = Q(from_date__gte=from_date, from_date__lt=until_date)
        until_in_range_filter = Q(until_date__lte=until_date, until_date__gt=from_date)
        range_in_middle_filter = Q(from_date__lte=from_date, until_date__gte=until_date)

        if Booking.objects.filter(
            from_in_range_filter | until_in_range_filter | range_in_middle_filter, room_id=self.room_id
        ).exists():
            raise ValidationError(_('Esta habitación ya se encuentra reservada para esa fecha.'))

        # Populate the prices if it is missing
        if not self.price:
            diff_days = get_diff_between_tow_dates(self.from_date, self.until_date)
            self.price = self.room.price * diff_days

        super(Booking, self).save(*args, **kwargs)
