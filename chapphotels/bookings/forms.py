from django import forms
from .models import Booking
from django.utils.translation import gettext as _


class BookingForm(forms.ModelForm):

    class Meta:
        model = Booking
        fields = (
            'room', 'from_date', 'until_date', 'phone',
            'card_number', 'address', 'observations', 'paid'
        )

    def clean_until_date(self):
        from_date = self.cleaned_data.get('from_date')
        until_date = self.cleaned_data.get('until_date')

        if from_date > until_date:
            raise forms.ValidationError(
                _('La fecha de entrada no puede ser mayor a la de salida.')
            )

        return until_date

    def clean_paid(self):
        card_number = self.cleaned_data.get('card_number')

        if card_number:
            self.instance.paid = True

        return self.instance.paid
