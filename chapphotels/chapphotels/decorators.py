from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect


def login_required(view_func):
    def wrap(request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('login', permanent=False)

        return view_func(request, *args, **kwargs)
    return wrap