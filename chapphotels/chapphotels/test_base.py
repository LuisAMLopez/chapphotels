from django.test import TestCase
from rooms.models import Room
from django.contrib.auth.models import User
from decimal import Decimal
from django.urls import reverse
from django.utils import timezone
from datetime import timedelta
from bookings.models import Booking


class TestsBase(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'
        }
        self.user = User(username=self.credentials.get('username'))
        self.user.set_password(self.credentials.get('password'))
        self.user.save()

        self.room_1 = Room.objects.create(
            room_number=1, room_type=Room.SINGLE_ROOM,
            price=Decimal(40), smoker=False,
            wifi=True, air_conditioner=True,
            safe_box=False, fridge=False,
            viewer=True
        )

        self.room_2 = Room.objects.create(
            room_number=2, room_type=Room.SINGLE_ROOM,
            price=Decimal(50), smoker=False,
            wifi=True, air_conditioner=True,
            safe_box=True, fridge=False,
            viewer=True
        )

        self.room_3 = Room.objects.create(
            room_number=3, room_type=Room.DOUBLE_ROOM,
            price=Decimal(70), smoker=True,
            wifi=True, air_conditioner=True,
            safe_box=True, fridge=True,
            viewer=False
        )

    def signin(self):
        response = self.client.post('/accounts/login/', self.credentials, follow=True)
        self.assertTrue(response.context['user'].is_authenticated)
        return response.context['user']

    def create_bookings(self):
        room_4 = Room.objects.create(
            room_number=4, room_type=Room.SUITE_ROOM,
            price=Decimal(200), smoker=True,
            wifi=True, air_conditioner=True,
            safe_box=True, fridge=True,
            viewer=True
        )

        base_from_date = timezone.now().replace(hour=12, minute=0, second=0)
        base_until_date = base_from_date + timedelta(days=2)

        Booking.objects.create(
            room=self.room_1, user=self.user,
            from_date=base_from_date, until_date=base_until_date,
            paid=True, price=Decimal(600)
        )

        Booking.objects.create(
            room=room_4, user=self.user,
            from_date=base_from_date,
            until_date=base_until_date  + timedelta(days=2),
            paid=True, price=Decimal(600)
        )
                