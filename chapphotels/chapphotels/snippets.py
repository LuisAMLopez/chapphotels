import math
from time import mktime


def datetime_to_ms(dt):
    return 1000.0 * mktime(dt.timetuple())


def get_diff_between_tow_dates(dt1, dt2):
    time_diff = abs(datetime_to_ms(dt2) - datetime_to_ms(dt1))
    diff_days = math.ceil(time_diff / (1000 * 3600 * 24))
    return diff_days
