# -*- encoding: utf-8 -*-
from django.shortcuts import render
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import CreateView
from django.contrib.auth.models import User
from .forms import UserSignupForm
from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.shortcuts import redirect


def home(request):
    # return render(request, 'index.html')
    return redirect('room-list', permanent=True)


class UserSignUp(SuccessMessageMixin, CreateView):
    model = User
    form_class = UserSignupForm
    success_url = reverse_lazy('home')
    success_message = "Usuario creado correctamente"
    template_name = "registration/signup.html"

    def form_valid(self, form):
        super(UserSignUp,self).form_valid(form)        
        # The form is valid, automatically sign-in the user
        user = authenticate(
            self.request, username=form.cleaned_data['username'], 
            password=form.cleaned_data['password1']
        )

        if not user:
            # User not validated for some reason, return standard form_valid() response
            return self.render_to_response(self.get_context_data(form=form))            
        else:
            # Log the user in
            login(self.request, user)
            # Redirect to success url
            return HttpResponseRedirect(self.get_success_url())
