from django.shortcuts import render
from django.views.generic.list import ListView
from .models import Room
from django.db.models import Q
from .forms import RoomListForm


class RoomListView(ListView):

    model = Room
    context_object_name = 'rooms'
    template_name = 'rooms/room_list.html'
    filter_form = None

    def dispatch(self, request):
        self.filter_form = RoomListForm(request.GET)
        return super(RoomListView, self).dispatch(request=request)

    def get(self, request, *args, **kwargs):
        '''
            Overwirte this method to include form data into context
        '''
        self.object_list = self.get_queryset()
        allow_empty = self.get_allow_empty()
        if not allow_empty:
            # When pagination is enabled and object_list is a queryset,
            # it's better to do a cheap query than to load the unpaginated
            # queryset in memory.
            if self.get_paginate_by(self.object_list) is not None and hasattr(self.object_list, 'exists'):
                is_empty = not self.object_list.exists()
            else:
                is_empty = len(self.object_list) == 0
            if is_empty:(_("Empty list and '%(class_name)s.allow_empty' is False.") % {
                    'class_name': self.__class__.__name__,
                })

        context = self.get_context_data()
        context['form'] = self.filter_form
        return self.render_to_response(context)

    def get_queryset(self):
        if self.filter_form.has_changed() and self.filter_form.is_valid():
            from_date = self.filter_form.cleaned_data.get('from_date')
            until_date = self.filter_form.cleaned_data.get('until_date')
            smoker = self.filter_form.cleaned_data.get('smoker')

            from_in_range_filter = Q(booking__from_date__gte=from_date, booking__from_date__lt=until_date)
            until_in_range_filter = Q(booking__until_date__lte=until_date, booking__until_date__gt=from_date)
            range_in_middle_filter = Q(booking__from_date__lte=from_date, booking__until_date__gte=until_date)

            if smoker:
                smoker_filter = Q(smoker=smoker)
            else:
                smoker_filter = Q()

            return super(RoomListView, self).get_queryset().exclude(
                from_in_range_filter | until_in_range_filter | range_in_middle_filter
            ).filter(smoker_filter)

        return super(RoomListView, self).get_queryset()
