# -*- encoding: utf-8 -*-
from django import forms
from django.utils import timezone
from datetime import timedelta


class RoomListForm(forms.Form):
    from_date = forms.DateTimeField(required=False, widget=forms.DateInput(), initial=timezone.now())
    until_date = forms.DateTimeField(
        required=False, widget=forms.DateInput(), initial=timezone.now() + timedelta(days=5)
    )
    smoker = forms.BooleanField(required=False, label='Habitación fumadores')

    def clean(self):
        cleaned_data = super(RoomListForm, self).clean()

        from_date = cleaned_data.get('from_date')
        until_date = cleaned_data.get('until_date')

        if not from_date:
            from_date = timezone.now()

        if not until_date:
            until_date = timezone.now() + timedelta(days=5)

        from_date = from_date.replace(hour=12, minute=0, second=0)
        until_date = until_date.replace(hour=12, minute=0, second=0)

        self.fields.get('from_date').initial = from_date
        self.fields.get('until_date').initial = until_date

        self.initial['from_date'] = from_date
        self.initial['until_date'] = until_date

        cleaned_data.update({
            'from_date': from_date,
            'until_date': until_date
        })

        return cleaned_data
