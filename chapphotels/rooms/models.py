# -*- encoding: utf-8 -*-
from django.db import models


class Room(models.Model):
    SINGLE_ROOM = 1
    DOUBLE_ROOM = 2
    EXTRA_BED_ROOM = 3
    TRIPLE_ROOM = 4
    SUITE_ROOM = 5
    MATRIMONIAL_SUITE_ROOM = 6

    RoomTypeChoices = (
        (SINGLE_ROOM, 'Habitación simple'),
        (DOUBLE_ROOM, 'Habitación doble'),
        (EXTRA_BED_ROOM, 'Habitación con cama supletoria'),
        (TRIPLE_ROOM, 'Habitación triple'),
        (SUITE_ROOM, 'Suite'),
        (MATRIMONIAL_SUITE_ROOM, 'Suite matrimonial')
    )

    room_number = models.IntegerField('Número de habitación', primary_key=True, unique=True)
    room_type = models.IntegerField('Tipo de habitación', choices=RoomTypeChoices)
    price = models.DecimalField('Precio por noche (€)', max_digits=19, decimal_places=10)
    image = models.ImageField('Imagen', upload_to='room_images/', blank=True, null=True)
    smoker = models.BooleanField('Habitación para fumadores', default=False)
    wifi = models.BooleanField('WIFI', default=False)
    air_conditioner = models.BooleanField('Aire acondicionado', default=False)
    safe_box = models.BooleanField('Caja fuerte', default=False)
    fridge = models.BooleanField('Nevera', default=False)
    viewer = models.BooleanField('Balcón', default=False)

    class Meta:
        verbose_name_plural = "Habitaciones"
        verbose_name = "Habitación"

    def get_room_type_label(self):
        for room_type in self.RoomTypeChoices:
            if self.room_type == room_type[0]:
                return room_type[1]

        return ''

