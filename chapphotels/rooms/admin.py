# -*- encoding: utf-8 -*-
from django.contrib import admin
from .models import Room
from django.conf import settings
from django.utils.safestring import mark_safe


class RoomAdmin(admin.ModelAdmin):
    list_per_page = 25
    list_display = (
        'room_number', 'room_type', 'get_price', 'get_image',
        'smoker', 'wifi', 'air_conditioner', 'safe_box',
        'fridge', 'viewer',
    )
    readonly_fields = ('get_image',)

    def get_image(self, obj):
        try:
            image_path = settings.MEDIA_ROOT + obj.image.url
            image_html = '<img src="{0}">'.format(image_path)
            return mark_safe(image_html)
        except Exception:
            return ''

    get_image.short_description = 'Imagen'

    def get_price(self, obj):
        return "{0:.2f} €".format(obj.price)

    get_price.short_description = 'Precio'


admin.site.register(Room, RoomAdmin)
