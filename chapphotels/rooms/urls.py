from django.urls import path
from .views import RoomListView

urlpatterns = [
	path('list/', RoomListView.as_view(), name="room-list"),
]