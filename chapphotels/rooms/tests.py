from chapphotels.test_base import TestsBase
from rooms.models import Room
from bookings.models import Booking
from decimal import Decimal
from django.urls import reverse
from django.utils import timezone
from datetime import datetime, timedelta
from django.db.models import Q


class Tests(TestsBase):
    def setUp(self):
        super(Tests, self).setUp()

    def test_list_all_rooms(self):
        self.signin()

        expected_rooms = Room.objects.all()

        response = self.client.get(reverse('room-list'))
        self.assertEquals(response.status_code, 200)

        rooms = response.context_data.get('rooms')
        self.assertEquals(rooms.count(), expected_rooms.count())

    def test_filter_available_rooms(self):
        '''
            Create some bookings and test booking rooms are not in room list
            in a range on dates 
        '''
        self.signin()
        self.create_bookings()

        from_date = timezone.now()
        until_date = from_date + timedelta(days=5)

        from_in_range_filter = Q(booking__from_date__gte=from_date, booking__from_date__lt=until_date)
        until_in_range_filter = Q(booking__until_date__lte=until_date, booking__until_date__gt=from_date)
        range_in_middle_filter = Q(booking__from_date__lte=from_date, booking__until_date__gte=until_date)

        expected_rooms =  Room.objects.exclude(
            from_in_range_filter | until_in_range_filter | range_in_middle_filter
        )

        request_data = {
            'from_date': from_date.strftime('%Y-%m-%d %H:%H:%S'),
            'until_date': until_date.strftime('%Y-%m-%d %H:%H:%S')
        }
        response = self.client.get(reverse('room-list'), request_data)
        self.assertEquals(response.status_code, 200)

        rooms = response.context_data.get('rooms')
        self.assertEquals(rooms.count(), expected_rooms.count())

        for room in rooms:
            expected_rooms.get(pk=room.pk)

    def test_filter_available_rooms_by_smoker(self):
        '''
            Create some bookings and test booking rooms are not in room list
            in a range on dates and filter list by smoke field
        '''
        self.signin()
        self.create_bookings()

        from_date = timezone.now()
        until_date = from_date + timedelta(days=5)

        from_in_range_filter = Q(booking__from_date__gte=from_date, booking__from_date__lt=until_date)
        until_in_range_filter = Q(booking__until_date__lte=until_date, booking__until_date__gt=from_date)
        range_in_middle_filter = Q(booking__from_date__lte=from_date, booking__until_date__gte=until_date)
        smoke_filter = Q(smoker=True)

        expected_rooms =  Room.objects.exclude(
            from_in_range_filter | until_in_range_filter | range_in_middle_filter
        ).filter(smoke_filter)

        request_data = {
            'from_date': from_date.strftime('%Y-%m-%d %H:%H:%S'),
            'until_date': until_date.strftime('%Y-%m-%d %H:%H:%S'),
            'smoker': True
        }
        response = self.client.get(reverse('room-list'), request_data)
        self.assertEquals(response.status_code, 200)

        rooms = response.context_data.get('rooms')
        self.assertEquals(rooms.count(), expected_rooms.count())

        for room in rooms:
            expected_rooms.get(pk=room.pk)
