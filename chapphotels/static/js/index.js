/*
 * This function open a modal with a booking form
 * to create a booking
 */
function bookingButtonClick(room_number, price) {
	var room_title = $('div#room-'+room_number+'-id h5.card-title').text();
	var from_date = $('input#id_from_date').val();
	var until_date = $('input#id_until_date').val();

	// Get difference between from and until dates
	var fromDate = new Date(from_date);
	var untilDate = new Date(until_date);
	var timeDiff = Math.abs(untilDate.getTime() - fromDate.getTime());
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

	var totalRoomPrice = price * diffDays;

	// Set basic information to modal form
	$('input#room_modal_id').val(room_number);
	$('h5#exampleModalLongTitle').html(room_title);
	$('input#from_date_modal').val(from_date);
	$('input#until_date_modal').val(until_date);
	$('label#total_label_id').html(parseFloat(totalRoomPrice.toFixed(2)));

	// Open modal
	$('#bookingCreateModal').modal()
}

/*
* This function calls detail service and set the returned html in a modal
*/
function bookingDetailButtonClick(pk) {
	var laber_url = 'label#bookingDetailUrl_' + pk; 
	var url = $(laber_url).text();
	$('div#bookingDetailDiv').load(url, function (responseText, textStatus, jqXHR) {
        // Open detail modal
        $('#bookingDetailModal').modal()
	});
}


/*
* Initialize tooltips
*/
$(function () {
	$('[data-toggle="tooltip"]').tooltip();
})